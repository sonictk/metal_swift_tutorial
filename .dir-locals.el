(
 (nil . ((tab-width . 4)
         (indent-tabs-mode . nil)))

 (c++-mode . ((c-basic-offset . 4)
              (tab-width . 4)
              (indent-tabs-mode . nil)
              (compile-command . "./build.sh debug")
              (cd-compile-directory . "/Users/sonictk/Git/experiments/metal_swift_tutorial")
              (cc-search-directories . ("."
                                        " /usr/local/include"
                                        "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/../include/c++/v1"
                                        "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/11.0.0/include"
                                        "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include"
                                        "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/usr/include"
                                        "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/System/Library/Frameworks"
                                        ))
              ))

 (c-mode . ((c-basic-offset . 4)
            (tab-width . 4)
            (indent-tabs-mode . nil)
            (compile-command . "./build.sh debug")
            (cd-compile-directory . "/Users/sonictk/Git/experiments/metal_swift_tutorial")
            (cc-search-directories . ("."
                                      " /usr/local/include"
                                      "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/../include/c++/v1"
                                      "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/11.0.0/include"
                                      "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include"
                                      "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/usr/include"
                                      "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/System/Library/Frameworks"
                                      ))
            ))

 (objc-mode . ((c-basic-offset . 4)
               (tab-width . 4)
               (indent-tabs-mode . nil)
               (compile-command . "./build.sh debug")
               (cd-compile-directory . "/Users/sonictk/Git/experiments/metal_swift_tutorial")
               (cc-search-directories . ("."
                                         " /usr/local/include"
                                         "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/../include/c++/v1"
                                         "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/11.0.0/include"
                                         "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include"
                                         "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/usr/include"
                                         "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/System/Library/Frameworks"
                                         ))
               ))

 (swift-mode . ((c-basic-offset . 4)
                (tab-width . 4)
                (indent-tabs-mode . nil)
                (compile-command . "./build.sh debug")
                (cd-compile-directory . "/Users/sonictk/Git/experiments/metal_swift_tutorial")
                (cc-search-directories . ("."
                                          " /usr/local/include"
                                          "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/bin/../include/c++/v1"
                                          "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/lib/clang/11.0.0/include"
                                          "/Applications/Xcode.app/Contents/Developer/Toolchains/XcodeDefault.xctoolchain/usr/include"
                                          "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/usr/include"
                                          "/Library/Developer/CommandLineTools/SDKs/MacOSX10.14.sdk/System/Library/Frameworks"
                                          ))
                ))
 )
