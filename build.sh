#!/usr/bin/env bash

# This is the build script for the Swift Metal tutorial.
# usage: build.sh [debug|release|clean]

# todo: look into using "`dirname \"$0\"`" to act as replacement for $PWD.

StartTime=`date +%T`;
echo "Build script started executing at ${StartTime}...";

# Process command line arguments
BuildType=$1;

if [ "${BuildType}" == "" ]; then
    BuildType="release";
fi;

ProjectName=$2;
if [ "${ProjectName}" == "" ]; then
    ProjectName="swiftmetal";
fi;

echo "Building ${ProjectName} in ${BuildType} configuration...";

# Define colours to be used for terminal output messages
RED='\033[0;31m';
GREEN='\033[0;32m';
NC='\033[0m'; # No Color

# Create a build directory to store artifacts
BuildDir="${PWD}/osxbuild";
AppBuildDir="${BuildDir}/objcmetal.app";

# If cleaning builds, just delete build artifacts and exit immediately
if [ "${BuildType}" == "clean" ]; then
    echo "Cleaning build from directory: ${BuildDir}. Files will be deleted!";
    read -p "Continue? (Y/N)" ConfirmCleanBuild;
    if [ $ConfirmCleanBuild == [Yy] ]; then
       echo "Removing files in: ${BuildDir}...";
       rm -rf $BuildDir;
    fi;

    exit 0;
fi;

echo "Building in directory: ${BuildDir}";
if [ ! -d "${BuildDir}" ]; then
   mkdir -p "${BuildDir}";
fi;

echo "Output app bundle: ${AppBuildDir}";
if [ ! -d "${AppBuildDir}" ]; then
   mkdir -p "${AppBuildDir}";
fi;

ResourcesDirPath="${PWD}/resources";
InfoPListFilePath="${ResourcesDirPath}/Info.plist";

swiftcPath="swiftc";
SwiftEntryPoint="${PWD}/src/main.swift";
SwiftCompilerFlagsDebug="-g";
SwiftCompilerFlagsRelease="";
OutSwiftExe="${BuildDir}/${ProjectName}";

clangPath="clang";
ObjCEntryPoint="${PWD}/src/main.mm";
ClangObjCCompilerFlags="-std=c++14";
ClangObjCCompilerFlagsDebug="${ClangObjCCompilerFlags} -g -O0";
ClangObjCCompilerFlagsRelease="${ClangObjCCompilerFlags} -O3";
ClangLinkerFlags="-lstdc++ -framework AppKit -framework QuartzCore -framework Metal -framework MetalKit -framework IOKit -framework GameController";
OutObjCExe="${BuildDir}/objcmetal";

metalShadersEntryPoint="${PWD}/src/osx_shaders.metal";
metalCmd="xcrun -sdk macosx metal";
metalCompilerFlags="-c";
# TODO: why does the metal compiler crash if the shaders are compiled with -O0? It makes _zero_ sense!!!
metalCompilerFlagsDebug="${metalCompilerFlags} -g";
metalCompilerFlagsRelease="${metalCompilerFlags}";
metalArCmd="xcrun -sdk macosx metal-ar";
metallibCmd="xcrun -sdk macosx metallib";
OutMetalIR="${BuildDir}/osx_shaders.air"
OutMetallib="${BuildDir}/osx_shaders.metallib";

if [ "${BuildType}" == "debug" ]
then
    BuildSwiftCommand="${swiftcPath} ${SwiftEntryPoint} ${SwiftCompilerFlagsDebug} -o ${OutSwiftExe}";
    BuildObjCCommand="${clangPath} ${ClangObjCCompilerFlagsDebug} ${ClangLinkerFlags} ${ObjCEntryPoint} -o ${OutObjCExe}";
    BuildMetalIRCommand="${metalCmd} ${metalCompilerFlagsDebug} -o ${OutMetalIR} ${metalShadersEntryPoint}"
else
    BuildSwiftCommand="${swiftcPath} ${SwiftEntryPoint} ${SwiftCompilerFlagsRelease} -o ${OutSwiftExe}";
    BuildObjCCommand="${clangPath} ${ClangObjCCompilerFlagsRelease} ${ClangLinkerFlags} ${ObjCEntryPoint} -o ${OutObjCExe}";
    BuildMetalIRCommand="${metalCmd} ${metalCompilerFlagsRelease} -o ${OutMetalIR} ${metalShadersEntryPoint}"
fi;

BuildMetallibCommand="${metallibCmd} -o ${OutMetallib} ${OutMetalIR}";

echo "Compiling/linking Swift source files (command follows below)...";
echo "${BuildSwiftCommand}";
echo "";
${BuildSwiftCommand};

if [ $? -ne 0 ]; then
    echo -e "${RED}***************************************${NC}";
    echo -e "${RED}*      !!! An error occurred!!!       *${NC}";
    echo -e "${RED}***************************************${NC}";

    EndTime=`date +%T`;
    echo "Build script finished execution at ${EndTime}.";

    exit 1;
fi;

echo "Compiling/linking Objective-C source files (command follows below)...";
echo "${BuildObjCCommand}";
echo "";
${BuildObjCCommand};

if [ $? -ne 0 ]; then
    echo -e "${RED}***************************************${NC}";
    echo -e "${RED}*      !!! An error occurred!!!       *${NC}";
    echo -e "${RED}***************************************${NC}";

    EndTime=`date +%T`;
    echo "Build script finished execution at ${EndTime}.";

    exit 1;
fi;

echo "Compiling Metal shaders... (command follows below)...";
echo "${BuildMetalIRCommand}";
echo "";
${BuildMetalIRCommand};

if [ $? -ne 0 ]; then
    echo -e "${RED}***************************************${NC}";
    echo -e "${RED}*      !!! An error occurred!!!       *${NC}";
    echo -e "${RED}***************************************${NC}";

    EndTime=`date +%T`;
    echo "Build script finished execution at ${EndTime}.";

    exit 1;
fi;

echo "Creating Metal library... (command follows below)...";
echo "${BuildMetallibCommand}";
echo "";
${BuildMetallibCommand};

if [ $? -ne 0 ]; then
    echo -e "${RED}***************************************${NC}";
    echo -e "${RED}*      !!! An error occurred!!!       *${NC}";
    echo -e "${RED}***************************************${NC}";

    EndTime=`date +%T`;
    echo "Build script finished execution at ${EndTime}.";

    exit 1;
fi;

echo "Copying resources...";
cp ${InfoPListFilePath} ${AppBuildDir};
cp ${OutObjCExe} ${AppBuildDir};
cp ${OutMetallib} ${AppBuildDir};


# On success, display confirmation message.
echo -e "${GREEN}***************************************${NC}";
echo -e "${GREEN}*    Build completed successfully!    *${NC}";
echo -e "${GREEN}***************************************${NC}";


EndTime=`date +%T`;
echo "Build script finished execution at ${EndTime}.";

exit 0;
