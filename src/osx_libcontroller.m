#import "osx_libcontroller.h"
#import <IOKit/hid/IOHIDLib.h>

static IOHIDManagerRef gIOHIDMgrRef = NULL;
static OSXLibController *gController = nil;


@implementation OSXLibController
{
    CFIndex dpadLUsageID;
    CFIndex dpadRUsageID;
    CFIndex dpadDUsageID;
    CFIndex dpadUUsageID;

    CFIndex btnAUsageID;
    CFIndex btnBUsageID;
    CFIndex btnXUsageID;
    CFIndex btnYUsageID;

    CFIndex btnLShdrUsageID;
    CFIndex btnRShdrUsageID;
}

static void controllerInput(void *context, IOReturn result, void *sender, IOHIDValueRef value)
{
    if (result != kIOReturnSuccess) {
        return;
    }

    // NOTE: (sonictk) Apparently usage pages are important to figuring out when we're modifying button state
    // only when a button is pressed instead of the dpad or analog stick.
    OSXLibController *controller = (__bridge OSXLibController *)context;
    IOHIDElementRef element = IOHIDValueGetElement(value);
    uint32_t usagePage = IOHIDElementGetUsagePage(element);
    uint32_t usage = IOHIDElementGetUsage(element);

    if (usagePage == kHIDPage_Button) {
        // NOTE: (sonictk) Button presses
        BOOL btnState = (BOOL)IOHIDValueGetIntegerValue(value);
        if (usage == controller->btnAUsageID) {
            controller->_buttonAState = btnState;
        } else if (usage == controller->btnBUsageID) {
            controller->_buttonBState = btnState;
        } else if (usage == controller->btnXUsageID) {
            controller->_buttonXState = btnState;
        } else if (usage == controller->btnYUsageID) {
            controller->_buttonYState = btnState;
        } else if (usage == controller->btnLShdrUsageID) {
            controller->_buttonLfShoulderState = btnState;
        } else if (usage == controller->btnRShdrUsageID) {
            controller->_buttonRtShoulderState = btnState;
        }

    } else if (usagePage == kHIDPage_GenericDesktop && usage == kHIDUsage_GD_Hatswitch) {
        // NOTE: (sonictk) D-pad presses.
        int dpadState = (int)IOHIDValueGetIntegerValue(value);

        NSInteger dpadX = 0;
        NSInteger dpadY = 0;

        switch (dpadState) {
        case 0:
            dpadX = 0;
            dpadY = 1;
            break;
        case 1:
            dpadX = 1;
            dpadY = 1;
            break;
        case 2:
            dpadX = 1;
            dpadY = 0;
            break;
        case 3:
            dpadX = 1;
            dpadY = -1;
            break;
        case 4:
            dpadX = 0;
            dpadY = -1;
            break;
        case 5:
            dpadX = -1;
            dpadY = -1;
            break;
        case 6:
            dpadX = -1;
            dpadY = 0;
            break;
        case 7:
            dpadX = -1;
            dpadY = 1;
            break;
        default:
            dpadX = 0;
            dpadY = 0;
            break;
        }

        controller->_dPadX = dpadX;
        controller->_dPadY = dpadY;
    }

    return;
}

static void controllerConnected(void *context, IOReturn result, void *sender, IOHIDDeviceRef device)
{
    if (result != kIOReturnSuccess) {
        return;
    }

    NSUInteger vendorID = [(__bridge NSNumber *)IOHIDDeviceGetProperty(device, CFSTR(kIOHIDVendorIDKey)) unsignedIntegerValue];
    NSUInteger productID = [(__bridge NSNumber *)IOHIDDeviceGetProperty(device, CFSTR(kIOHIDProductIDKey)) unsignedIntegerValue];

    OSXLibController *controller = [[OSXLibController alloc] init];

    if (vendorID == XBOX_ONE_CONTROLLER_VENDOR_ID && productID == XBOX_ONE_CONTROLLER_PRODUCT_ID) {
        // TODO: (sonictk) Don't know what the real IDs are for the XBox One controller, so we're just guessing here.
        controller->btnAUsageID = 0x02;
        controller->btnBUsageID = 0x03;
        controller->btnXUsageID = 0x01;
        controller->btnYUsageID = 0x04;
        controller->btnLShdrUsageID = 0x05;
        controller->btnRShdrUsageID = 0x06;
    }

    IOHIDDeviceRegisterInputValueCallback(device, controllerInput, (__bridge void *)controller);
    IOHIDDeviceSetInputValueMatchingMultiple(device, (__bridge CFArrayRef)@[
                                                 @{@(kIOHIDElementUsagePageKey): @(kHIDPage_GenericDesktop)},
                                                 @{@(kIOHIDElementUsagePageKey): @(kHIDPage_Button)}
                                                 ]);

    gController = controller;

    return;
}

+ (void)initialize
{
    gIOHIDMgrRef = IOHIDManagerCreate(kCFAllocatorDefault, 0);
    gController = [[OSXLibController alloc] init];

    if (IOHIDManagerOpen(gIOHIDMgrRef, kIOHIDOptionsTypeNone) != kIOReturnSuccess) {
        return;
    }

    IOHIDManagerRegisterDeviceMatchingCallback(gIOHIDMgrRef, controllerConnected, NULL);
    IOHIDManagerSetDeviceMatchingMultiple(gIOHIDMgrRef, (__bridge CFArrayRef)@[
                                              @{@(kIOHIDDeviceUsagePageKey): @(kHIDPage_GenericDesktop), @(kIOHIDDeviceUsageKey) : @(kHIDUsage_GD_GamePad)},
                                              @{@(kIOHIDDeviceUsagePageKey): @(kHIDPage_GenericDesktop), @(kIOHIDDeviceUsageKey) : @(kHIDUsage_GD_MultiAxisController)}
                                              ]);

    IOHIDManagerScheduleWithRunLoop(gIOHIDMgrRef, CFRunLoopGetMain(), kCFRunLoopDefaultMode);

    return;
}

+ (OSXLibController *)connectedController
{
    return gController;
}

@end
