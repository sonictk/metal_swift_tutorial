#import <Foundation/Foundation.h> // The compiler keeps track of whether the imported file has been used previously, unlike the #include directive.
#import <AppKit/AppKit.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>
#import <QuartzCore/QuartzCore.h>

#include <mach/mach.h>
#include <mach/mach_time.h>
#include <stdio.h>

#import "osx_shaderdefs.h"
#import "osx_libmetal.m"
#import "osx_libcontroller.m"
#import "osx_libgccontroller.m"
#import "osx_libwindows.m"


static int gWndWidth = 1280;
static int gWndHeight = 800;

static int gBmpWidth = 0;
static int gBmpHeight = 0;
static int gPitch = 0;
static int gBytesPerPixel = 4;

static uint8_t *gDrawBuffer = NULL;

static bool gGameIsRunning = true;

static int gOffsetX = 0;
static int gOffsetY = 0;

static uint64_t gStartTime = 0;


// Equivalent of the win32 WndProcCallback messaging system.
@interface gMainWindowDelegate: NSObject<NSWindowDelegate>
@end

@implementation gMainWindowDelegate
- (void)windowWillClose: (id)sender
{
    gGameIsRunning = false;
}

- (void)windowDidResize: (NSNotification *)notification
{
    // Resizes the window buffer to account for end-user resizing the window.
    NSWindow *window = (NSWindow *)notification.object;
    // macOSRefreshBuffer(window);
    // renderWeirdGradient();
}
@end


int main(int argc, char *argv[])
{
    gStartTime = mach_absolute_time();

    // MacOS screen coords are reversed of iOS; they are bottom to top. 0 is bottom.
    NSRect screenRect = [[NSScreen mainScreen] frame];

    // Render the window in the center of the screen.
    NSRect wndFrame = NSMakeRect((screenRect.size.width - (float)gWndWidth) * 0.5,
                                 (screenRect.size.height - (float)gWndHeight) * 0.5,
                                 gWndWidth,
                                 gWndHeight);

    NSWindow *window = [[MainGameWindow alloc] initWithContentRect: wndFrame
                                                         styleMask: NSWindowStyleMaskTitled|NSWindowStyleMaskClosable|NSWindowStyleMaskMiniaturizable|NSWindowStyleMaskResizable
                                                           backing: NSBackingStoreBuffered
                                                             defer: NO];

    gMainWindowDelegate *wndDelegate = [[gMainWindowDelegate alloc] init];
    [window setDelegate: wndDelegate];

    id<MTLDevice> mtlDevice = MTLCreateSystemDefaultDevice();
    TestMTLView *mtlView = [[TestMTLView alloc] initWithFrame:wndFrame
                                                       device:mtlDevice];
    window.contentView = mtlView;

    // Apparently this is important in order for drawing in the window to occur.
    window.contentView.wantsLayer = YES;

    // ``id`` is equivalent to ``void *`` in C, except that it always points to an objective-c object.

    [window setBackgroundColor: NSColor.magentaColor];
    [window setTitle: @"Metal Example Window"];
    // The "key window" is the one that receives keyboard events.
    [window makeKeyAndOrderFront: nil];

    // [OSXLibController initialize];
    // discoverWirelessControllers();

    // Main game loop.
    while (gGameIsRunning == true) {

        if (gController != nil) {
            // TODO: (sonictk) Hook up controller buttons to affect draw of final image buffer.
            if (gController.buttonAState == true) {
                gOffsetX++;
            }
        }

        NSEvent *event;
        do {
            event = [NSApp nextEventMatchingMask: NSEventMaskAny
                                       untilDate: nil
                                          inMode: NSDefaultRunLoopMode
                                         dequeue: YES];

            if (event != nil && (event.type == NSEventTypeKeyDown || event.type == NSEventTypeKeyUp)) {
                NSLog(@"Key pressed");
            }

            switch ([event type]) {
            default:
                [NSApp sendEvent: event];
            }
        } while (event != nil);
    }

    printf("Reached end of execution.");

    return 0;
}
