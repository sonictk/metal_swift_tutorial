#import "osx_libmetal.h"
#import "osx_shaderdefs.h"
#include <dispatch/dispatch.h>


constexpr int gUniformBufferCount = 3;

static const Vtx3 gTriVerts[] = {
    Vtx3{{-0.5, -0.5, 0}, {255, 0, 0, 255}},
    Vtx3{{0, 0.5, 0}, {0, 255, 0, 255}},
    Vtx3{{0.5, -0.5, 0}, {0, 0, 255, 255}}
};


// TODO: (sonictk) Implement custom NSView subclass to replace MTKView from the Metal toolkit.
@implementation MTLView
+ (Class)layerClass
{
    return [CAMetalLayer class];
}
@end


@implementation MTLRenderer
{
    id <MTLDevice> mtlDevice;
    id <MTLCommandQueue> cmdQueue;
    id <MTLRenderPipelineState> pipelineState;
    id <MTLBuffer> vtxBuf;
}

+ (id<MTLRenderPipelineState>)buildRenderPipeline:(id<MTLDevice>)device :(MTKView *)mtkView
{
    id<MTLRenderPipelineState> result;

    // TODO: (sonictk) Implement.

    return result;
}

- (id)init:(MTKView *)view
{
    return self;
}

- (void)mtkView:(MTKView *)view drawableSizeWillChange:(CGSize)size
{
    return;
}

- (void)drawInMTKView:(MTKView *)view
{

    return;
}
@end


@implementation TestMTLView
{
    id <MTLLibrary> mtlLib;
    id <MTLCommandQueue> mtlCmdQueue;
    id <MTLRenderPipelineState> mtlPipelineState;
    id <MTLDepthStencilState> mtlDepthState;
    id <MTLBuffer> uniformBuffers[gUniformBufferCount];
    id <MTLBuffer> vtxBuffer;
    int uniformBufferIndex;
    int frame;
    dispatch_semaphore_t semaphore;
}

- (id)initWithFrame:(CGRect)frameRect device:(id<MTLDevice>)device
{
    self = [super initWithFrame:frameRect
                         device:device];
    if (self) {
        [self setup];
    }

    return self;
}

- (void)setup
{
    self.colorPixelFormat = MTLPixelFormatBGRA8Unorm;
    self.depthStencilPixelFormat = MTLPixelFormatDepth32Float_Stencil8;

    // Load the pre-compiled shaders.
    NSError *error = nil;
    NSString *curPath = [[NSBundle mainBundle] bundlePath];
    NSString *metallibFilePath = [NSString stringWithFormat:@"%@/%@", curPath, @"osx_shaders.metallib"];
    self->mtlLib = [self.device newLibraryWithFile:metallibFilePath error:&error];
    if (!self->mtlLib) {
        NSLog(@"Failed to load shader library. Error %@", error);
        assert(0);
    }
    id <MTLFunction> vtxFunc = [self->mtlLib newFunctionWithName:@"vertexShader"];
    assert(vtxFunc != nil);
    id <MTLFunction> fragFunc = [self->mtlLib newFunctionWithName:@"fragmentShader"];
    assert(fragFunc != nil);

    // Depth-stencil state.
    MTLDepthStencilDescriptor *depthDesc = [MTLDepthStencilDescriptor new];
    depthDesc.depthCompareFunction = MTLCompareFunctionLess;
    depthDesc.depthWriteEnabled = YES;
    self->mtlDepthState = [self.device newDepthStencilStateWithDescriptor: depthDesc];

    // Vertex descriptor.
    MTLVertexDescriptor *vtxDesc = [MTLVertexDescriptor new];
    vtxDesc.attributes[VtxAttrs_Pos].format = MTLVertexFormatFloat3;
    vtxDesc.attributes[VtxAttrs_Pos].offset = 0;
    vtxDesc.attributes[VtxAttrs_Pos].bufferIndex = BufferIdx_MeshVtx;
    vtxDesc.attributes[VtxAttrs_Color].format = MTLVertexFormatUChar4;
    // The color starts after the position so we have to offset here.
    vtxDesc.attributes[VtxAttrs_Color].offset = sizeof(Vtx3::pos);
    vtxDesc.attributes[VtxAttrs_Color].bufferIndex = BufferIdx_MeshVtx;
    vtxDesc.layouts[BufferIdx_MeshVtx].stride = sizeof(Vtx3);
    vtxDesc.layouts[BufferIdx_MeshVtx].stepRate = 1;
    vtxDesc.layouts[BufferIdx_MeshVtx].stepFunction = MTLVertexStepFunctionPerVertex;

    // Pipeline state
    MTLRenderPipelineDescriptor *pipelineDesc = [MTLRenderPipelineDescriptor new];
    pipelineDesc.sampleCount = self.sampleCount;
    pipelineDesc.vertexFunction = vtxFunc;
    pipelineDesc.fragmentFunction = fragFunc;
    pipelineDesc.vertexDescriptor = vtxDesc;
    pipelineDesc.colorAttachments[0].pixelFormat = self.colorPixelFormat;
    pipelineDesc.depthAttachmentPixelFormat = self.depthStencilPixelFormat;
    pipelineDesc.stencilAttachmentPixelFormat = self.depthStencilPixelFormat;
    self->mtlPipelineState = [self.device newRenderPipelineStateWithDescriptor:pipelineDesc error:&error];
    if (!self->mtlPipelineState) {
        NSLog(@"Failed to create pipeline state. Error %@", error);
        assert(0);
    }
    self->vtxBuffer = [self.device newBufferWithBytes:gTriVerts
                                               length:sizeof(gTriVerts)
                                              options:MTLResourceStorageModeShared];

    // Create uniform buffers.
    for (int i=0; i < gUniformBufferCount; ++i) {
        self->uniformBuffers[i] = [self.device newBufferWithLength:sizeof(FrameUniforms)
                                                           options:MTLResourceCPUCacheModeWriteCombined];
    }
    self->frame = 0;

    // Create a semaphore for each uniform buffer.
    self->semaphore = dispatch_semaphore_create(gUniformBufferCount);
    self->uniformBufferIndex = 0;

    self->mtlCmdQueue = [self.device newCommandQueue];

    // self.clearColor = MTLClearColorMake(1.0, 0.0, 1.0, 1.0);

    return;
}

- (void)drawRect:(CGRect)rect
{
    // Wait for an available uniform buffer.
    dispatch_semaphore_wait(self->semaphore, DISPATCH_TIME_FOREVER);

    self->frame++;

    // Create rotation matrix to rotate the vertices over time.
    float rad = frame * 0.01f;
    float srad = sinf(rad);
    float crad = cosf(rad);
    simd::float4x4 rot44(simd::float4{crad, -srad, 0, 0},
                         simd::float4{srad, crad, 0, 0},
                         simd::float4{0, 0, 1, 0},
                         simd::float4{0, 0, 0, 1});

    // Update  the uniform buffer with animation.
    self->uniformBufferIndex = (self->uniformBufferIndex + 1) % gUniformBufferCount;
    FrameUniforms *uniforms = (FrameUniforms *)[self->uniformBuffers[self->uniformBufferIndex] contents];
    uniforms->projectionViewModel = rot44;

    id <MTLCommandBuffer> cmdBuf = [self->mtlCmdQueue commandBuffer];

    // Encode render command.
    id <MTLRenderCommandEncoder> encoder = [cmdBuf renderCommandEncoderWithDescriptor:self.currentRenderPassDescriptor];
    // Sets the viewport for transformations and clipping.
    [encoder setViewport:{0, 0, self.drawableSize.width, self.drawableSize.height, 0, 1 }];
    [encoder setDepthStencilState:self->mtlDepthState];
    [encoder setRenderPipelineState:self->mtlPipelineState];
    [encoder setVertexBuffer: self->uniformBuffers[self->uniformBufferIndex]
                      offset: 0
                     atIndex: BufferIdx_Uniform];
    [encoder setVertexBuffer: self->vtxBuffer
                      offset: 0
                     atIndex:BufferIdx_MeshVtx];
    [encoder drawPrimitives:MTLPrimitiveTypeTriangle
                vertexStart:0
                vertexCount:3];
    [encoder endEncoding];

    // Set callback for semaphore.
    // __block specifier means that values will be passed by reference to block functions.
    // I guess that's important for semaphores since that will mean that blocks acting
    // on the semaphore will act on the same memory.
    __block dispatch_semaphore_t semaphore = self->semaphore;

    // Once the GPU has completed its work for this buffer, release the
    // uniform buffer.
    [cmdBuf addCompletedHandler: ^(id<MTLCommandBuffer> buffer)
            {
                dispatch_semaphore_signal(self->semaphore);
            }];

    [cmdBuf presentDrawable: self.currentDrawable];
    [cmdBuf commit];

    // Draw children.
    [super drawRect:rect];

    return;
}
@end
