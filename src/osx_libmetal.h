/**
 * @file   osx_libmetal.h
 * @author Yi Liang Siew <sonictk@sonictk-mbp.local>
 * @date   Tue Nov  5 23:29:58 2019
 *
 * @brief
 *

 * @notes
 * In order to enable the metal validation layer:
 * export METAL_DEVICE_WRAPPER_TYPE=1 ; This is the most important
 * export METAL_ERROR_MODE=5
 * export METAL_DEBUG_ERROR_MODE=5
 *
 */

#ifndef OSX_LIBMETAL_H
#define OSX_LIBMETAL_H

#import <AppKit/AppKit.h>
#import <MetalKit/MetalKit.h>


/// Vertex structure on CPU memory.
struct Vtx3
{
    float pos[3];
    unsigned char color[4];
};


@interface MTLView : NSView
@end


@interface MTLRenderer: NSObject <MTKViewDelegate>
+ (id<MTLRenderPipelineState>)buildRenderPipeline:(id<MTLDevice>)device :(MTKView *)mtkView;

- (id)init:(MTKView *)view;

// This is called automatically every time the size of the MTKView changes.
- (void) mtkView:(MTKView *)view drawableSizeWillChange:(CGSize)size;

// Called automatically by MTKView whenever it wants new content to be rendered.
- (void)drawInMTKView:(MTKView *)view;
@end

#endif /* OSX_LIBMETAL_H */


@interface TestMTLView : MTKView
@end
