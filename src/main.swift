let a: Int = 40 // This is a constant.
var b: Float = 20.0 // This is a variable.
let arr: [Int] = [1, 2, 3, 4, 5]
let emptyArray = [Int]()
let emptyDict = [Int: String]()
let multiLineStr = """
This is a multiline string.
Not sure if it works.
"""

print("Hello world!" + String(20)) // Standard output.
print(multiLineStr)

var counter: Int = 0
// Looks like switch statements also work similarly to if else since they do not fall through cases, and
// are not limited to working with PODs. This must mean that they don't just optimize to jump tables.
for a in arr {
    if (a > 10) {
        counter += 1
    } else if (a < 10 ) {
        counter -= 1
    } else {

    }
}


// The underscore is a variable "label", meaning that it can be used unnamed. Otherwise you'd need to include the a:5 syntax
// when calling the function.
func foo(_ a: Int, _ b: Float) -> Float
{
    return Float(a) * b
}

print(foo(5, 4.0))

enum TestEnum: Int
{
    case First, Second, Third
}

// Structs are always copied nand Classes are passed by reference in Swift.
class TestClass
{
    var a: Int = 0
    var b: Float = 0.0
    var c: String

    init()
    {
        self.c = ""
    }

    deinit {}
}

struct TestStruct
{
    var a: Int
    var b: Float
    var c: String
}


// Kind of like C++ templates equivalent. Wonder how the codegen works.
func repeater<Item>(_ item: Item, _ numTimes: Int) -> Void
{
    for _ in 0..<numTimes {
        print(item)
    }
}

repeater("repeat", 5)
