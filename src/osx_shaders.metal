#include "osx_shaderdefs.h"
#include <metal_stdlib>
using namespace metal;


// struct VertexOut
// {
//     float4 color;
//     // This _needs_ to be float4, because the X-Y takes two components, depth
//     // takes the 3rd, and the last one is used to put the pos into 4D homogeneous space.
//     // all [[...]] indicate Metal-specific annotations.
//     float4 pos [[position]]; // [[position]] is a keyword for the rasterizer to know that this field is used for
//                              // reading the normalized screen-space pos. of vertices for performing rasterization/interpolation.
// };


// // [[buffer(0)]] corresponds to the index buffer when setting it for the render encoder.
// // It's how MTL knows that ``vertexArray`` is where it should pass the first buffer.
// // [[vertex_id]] tells MTL where to pass the vertex ID.
// vertex VertexOut vertexShader(const device Vtx *vertexArray [[buffer(0)]],
//                               unsigned int vid [[vertex_id]])
// {
//     Vtx in = vertexArray[vid];
//     VertexOut out;
//     out.color = in.color;
//     // Pass the already-normalized screen-space coords to the rasterizer.
//     out.pos = float4(in.pos.x, in.pos.y, 0, 1);

//     return out;
// }


// // [[stage_in]] means that Metal should feed the ``interp`` param the interpolated results of the rasterizer.
// fragment float4 fragmentShader(VertexOut interp [[stage_in]])
// {
//     return interp.color;
// }

struct VertexInput {
    float3 position [[attribute(VtxAttrs_Pos)]];
    half4 color [[attribute(VtxAttrs_Color)]];
};

struct ShaderInOut {
    float4 position [[position]];
    half4  color;
};

vertex ShaderInOut vertexShader(VertexInput in [[stage_in]], constant FrameUniforms& uniforms [[buffer(BufferIdx_Uniform)]]) {
    ShaderInOut out;
    float4 pos4 = float4(in.position, 1.0);
    out.position = uniforms.projectionViewModel * pos4;
    out.color = in.color / 255.0;
    return out;
}

fragment half4 fragmentShader(ShaderInOut in [[stage_in]]) {
    return in.color;
}
