#ifndef OSX_LIBCONTROLLER_H
#define OSX_LIBCONTROLLER_H

#import <AppKit/AppKit.h>


// NOTE: (sonictk) Retrieved by checking the System Report within the OSX About this Mac settings page.
#define XBOX_ONE_CONTROLLER_VENDOR_ID 0x045E
#define XBOX_ONE_CONTROLLER_PRODUCT_ID 0x02E0

@interface OSXLibController: NSObject
+ (OSXLibController *)connectedController; // + instead of - means that this variable is a class variable, not an instance one.

@property NSInteger dPadX;
@property NSInteger dPadY;

@property BOOL buttonAState;
@property BOOL buttonBState;
@property BOOL buttonXState;
@property BOOL buttonYState;

@property BOOL buttonLfShoulderState;
@property BOOL buttonRtShoulderState;

@end


#endif /* OSX_LIBCONTROLLER_H */
