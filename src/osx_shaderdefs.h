#ifndef OSX_SHADERDEFS_H
#define OSX_SHADERDEFS_H

#include <simd/simd.h>


enum VtxAttrs
{
    VtxAttrs_Pos = 0,
    VtxAttrs_Color = 1
};


enum BufferIdx
{
    BufferIdx_MeshVtx = 0,
    BufferIdx_Uniform = 1
};


struct FrameUniforms
{
    simd::float4x4 projectionViewModel;
};


#endif /* OSX_SHADERDEFS_H */
