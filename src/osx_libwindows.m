#import "osx_libwindows.h"

@implementation MainGameWindow
// NOTE: (sonictk) We override the window's key press event handler to make it do nothing so that
// MacOS doesn't keep beeping at us on key input.
- (void)keyDown: (NSEvent *)theEvent {}
@end
