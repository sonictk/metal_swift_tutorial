#import "osx_libgccontroller.h"
#import <GameController/GameController.h>


void (^wirelessControllerCompletionHandler)(void) = ^{
    NSLog(@"Wireless controller discovery finished.");

    return;
};


void discoverWirelessControllers()
{
    [GCController startWirelessControllerDiscoveryWithCompletionHandler: wirelessControllerCompletionHandler];

    return;
}
